import pandas as pd
import time
from sklearn.gaussian_process.kernels import (RBF, Matern, RationalQuadratic, ExpSineSquared, DotProduct, ConstantKernel)
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV

from besos.objectives import MeterReader
from besos import eppy_funcs as ef
import besos.sampling as sampling
from besos.problem import EPProblem
from besos.evaluator import EvaluatorEP
from besos.evaluator import EvaluatorSR
from besos.parameters import wwr, RangeParameter, FieldSelector, FilterSelector, GenericSelector, Parameter, expand_plist
from parameters import RangeParameter, CategoryParameter, expand_plist
import pickle
from parameter_sets import parameter_set

parameters = parameter_set(7)
objectives = [MeterReader('Electricity:Facility', name='Electricity Usage'),
              MeterReader('CO2:Facility', name='CO2')
             ]
problem = EPProblem(parameters, objectives)
building = ef.get_building()
evaluator = EvaluatorEP(problem, building)

inputs = sampling.dist_sampler(sampling.lhs, problem, 1000)
outputs = evaluator.df_apply(inputs)

#train_in, test_in, train_out, test_out = train_test_split(inputs, outputs, test_size=0.2)

hyperparameters = {'kernel':[None,1.0 * RBF(length_scale=1.0, length_scale_bounds=(1e-1, 10.0)),
                        1.0 * RationalQuadratic(length_scale=1.0, alpha=0.5),
                        1.0 * Matern(length_scale=1.0, length_scale_bounds=(1e-1, 10.0)),]}
folds = 3
gp = GaussianProcessRegressor(normalize_y=True)
clf = GridSearchCV(gp, hyperparameters, iid=True, cv=folds)
clf.fit(inputs, outputs)

with open(r'clf.pickle', 'wb') as output_file:
    pickle.dump(clf, output_file)

print("Update complete")

import dash_core_components as dcc
import dash_html_components as html
import dash_table
from dash import dash, no_update, callback_context
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate

import plotly
import chart_studio.plotly as py
import plotly.graph_objs as go
import matplotlib.pyplot as plt

import os
import pickle
import json
import time
import numpy as np
import pandas as pd

from sklearn.gaussian_process.kernels import (RBF, Matern, RationalQuadratic, ExpSineSquared, DotProduct, ConstantKernel)
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.model_selection import train_test_split, GridSearchCV

from besos import eppy_funcs as ef
import besos.sampling as sampling
from besos.problem import EPProblem
from besos.objectives import MeterReader
from besos.evaluator import EvaluatorEP, EvaluatorSR
from besos.parameters import wwr, RangeParameter, FieldSelector, FilterSelector, GenericSelector, Parameter, expand_plist
from parameters import RangeParameter, CategoryParameter, expand_plist

from parameter_set import parameter_set

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets = external_stylesheets)

EMPTY_GRAPH = {
    'layout': {
        'xaxis': {
            'showticklabels': False,
            'showgrid': False,
            'zeroline': False
        },
        'yaxis': {
            'showticklabels': False,
            'showgrid': False,
            'zeroline': False
        }
    }
}

OUTPUT_LIST = ['Electricity Usage', 'CO2']

app.layout = html.Div([
    html.Div([
        html.Div([
            html.Div([
                html.Label('Wall conductivity range'),
                dcc.RangeSlider(
                    id = 'wall_value',
                    step = 0.01,
                    marks = {range: f'{range:.2f}' for range in [0.02, 0.05, 0.1, 0.15, 0.2]},
                    min=0.02,
                    max=0.2,
                    value=[0.02, 0.2],
                    pushable = 0.01
                )
            ]),
            html.Div([
                html.Label('Attic thickness range'),
                dcc.RangeSlider(
                    id = 'attic_value',
                    step = 0.01,
                    marks = {range: f'{range:.2f}' for range in [0.1, 0.15, 0.2, 0.25, 0.3]},
                    min = 0.1,
                    max = 0.3,
                    value = [0.1, 0.3],
                    pushable = 0.01
                )
            ], style = {'margin-top': 20}),
            html.Div([
                html.Label('U-Factor range'),
                dcc.RangeSlider(
                    id = 'u_value',
                    step = 0.1,
                    marks = {range: f'{range:1.1f}' for range in [0.1, 1, 2, 3, 4, 5]},
                    min=0.1,
                    max=5,
                    value=[0.1, 5],
                    pushable = 0.1
                )
            ], style = {'margin-top': 25}),
            html.Div([
                html.Label('Solar Heat Gain Coefficient Range'),
                dcc.RangeSlider(
                    id = 'solar_value',
                    step = 0.01,
                    marks = {range: f'{range:.2f}' for range in [0.01, 0.2, 0.4, 0.6, 0.8, 0.99]},
                    min=0.01,
                    max=0.99,
                    value=[0.01, 0.99],
                    pushable = 0.01
                )
            ], style = {'margin-top': 25}),
            html.Div([
                html.Label('Electric Equipment Range'),
                dcc.RangeSlider(
                    id = 'electric_value',
                    step = 0.1,
                    marks={range: f'{range:.1f}' for range in [10, 11, 12, 13, 14, 15]},
                    min=10,
                    max=15,
                    value=[10, 15],
                    pushable = 0.1
                )
            ], style = {'margin-top': 25}),
            html.Div([
                html.Label('Light Range'),
                dcc.RangeSlider(
                    id = 'light_value',
                    step = 0.1,
                    marks={range: f'{range:.1f}' for range in [10, 11, 12, 13, 14, 15]},
                    min=10,
                    max=15,
                    value=[10, 15],
                    pushable = 0.1
                )
            ], style = {'margin-top': 25}),
            html.Div([
                html.Label('Window to Wall Ratio Range'),
                dcc.RangeSlider(
                    id = 'wwr_value',
                    step = 0.01,
                    marks = {range: f'{range:.2f}' for range in [0.01, 0.2, 0.4, 0.6, 0.8, 0.99]},
                    min=0.01,
                    max=0.99,
                    value=[0.01, 0.99],
                    pushable = 0.01
                )
            ], style = {'margin-top': 25})
        ]),
        html.Hr(),
        html.Div([
            html.Label('Input Range'),
            dcc.RadioItems(
                id = 'input_range',
                options = [
                    {'label': 'Fixed input range', 'value': 'fixed'},
                    {'label': 'Flex input range', 'value': 'flex'}
                ],
                value = 'fixed'
            )
        ]),
        html.Div([
            html.Label('Display output'),
            dcc.Checklist(
                id = 'output_parameter',
                options = [
                    {'label': 'Electricity Usage', 'value': 'Electricity Usage'},
                    {'label': 'CO2', 'value': 'CO2'},
                ],
                value = ['Electricity Usage']
            )
        ], style = {'margin-top': 20}),
        html.Div([
            html.Label('Number of inputs for evaluation'),
            dcc.Input(
                id = 'number_of_input',
                type = 'number',
                value = 5
            )
        ], style = {'margin-top': 20}),
        html.Button(
            'Re-run evaluation',
            id = 'run_button',
            className = 'ten columns',
            style = {'textAlign': 'center', 'margin-top': 30}
        )
    ], style = {'margin-left':80, 'margin-top': 30}, className = 'two columns'),
    html.Div([
        html.Div([
            dcc.Loading(
                id = 'loading_parcoord',
                children = [
                    dcc.Graph(
                        id = 'parcoord',
                        figure = EMPTY_GRAPH
                    )
                ],
                type = 'circle',
            )
        ], id = 'div_parcoord'),
        html.Div([
            dash_table.DataTable(
                id = 'datatable',
                sort_action = 'native',
                selected_columns = [],
                column_selectable = 'multi',
                page_size = 50,
                style_cell = {
                    'minWidth': '0px', 'width': '100px', 'maxWidth': '150px'
                },
                fixed_rows = {
                    'headers': True, 'data': 0
                }
            )
        ]),
        html.Div([
            dcc.Graph(
                id = 'scatter',
                figure = EMPTY_GRAPH
            )
        ]),
    ], className = 'nine columns'),
    html.Div(id = 'dataset', style = {'display': 'none'}),
    html.Div(id = 'sub_dataset', style = {'display': 'none'})
])

"""
    Run the evaluation with given input parameter range
    Then store the result into a invisible div
"""
@app.callback(
    Output('dataset', 'children'),
    [Input('run_button', 'n_clicks')],
    [State('number_of_input', 'value'),
    State('wall_value', 'value'),
    State('attic_value', 'value'),
    State('u_value', 'value'),
    State('solar_value', 'value'),
    State('electric_value', 'value'),
    State('light_value', 'value'),
    State('wwr_value', 'value')]
)
def run_model(run_click, number_of_input, wall_value, attic_value,
            u_value, solar_value, electric_value, light_value, wwr_value):
    #See example for Fit a Gaussian Process surrogate model in BESOS
    start_time = time.time()
    parameters = parameter_set(wall_value, attic_value, u_value, solar_value,
                                    electric_value, light_value, wwr_value)
    objectives = [MeterReader('Electricity:Facility', name='Electricity Usage'),
                  MeterReader('CO2:Facility', name='CO2')]
    problem = EPProblem(parameters, objectives)
    with open(r'clf.pickle', 'rb') as input_file:
        clf = pickle.load(input_file)

    def evaluation_func(ind):
        return ((clf.predict([ind])[0][0],clf.predict([ind])[0][1]),())
    GP_SM = EvaluatorSR(evaluation_func, problem)
    inputs = sampling.dist_sampler(sampling.lhs, problem, number_of_input)
    outputs = GP_SM.df_apply(inputs)
    datasets = {
        'inputs': inputs.to_json(orient = 'split', date_format = 'iso'),
        'outputs': outputs.to_json(orient = 'split', date_format = 'iso')
    }
    print("run model: ", time.time() - start_time)
    return json.dumps(datasets)

"""
    update parcoord
"""
@app.callback(
    Output('parcoord', 'figure'),
    [Input('dataset', 'children')],
    [State('input_range', 'value'),
    State('output_parameter', 'value')]
)
def update_parcoord(dataset, input_range, output_parameter):
    start_time = time.time()
    if dataset is None:
        raise PreventUpdate
    else:
        df = json.loads(dataset)
        inputs = pd.read_json(df['inputs'], orient = 'split').round(3)
        outputs = pd.read_json(df['outputs'], orient = 'split')
        results = inputs.join(outputs)
        l = list()
        for i in results.columns:
            if i not in OUTPUT_LIST or i in output_parameter:
                l.extend([dict(label = i, values = results[i])])

        if input_range == 'fixed':
            l = change_range(l)
        print("parcoord: ", time.time() - start_time)
        return {
            'data': [{
                'type': 'parcoords',
                'render_mode': 'webgl',
                'dimensions': l,
                'line': dict(color = results[results.columns[-1]])
            }],
        }

"""
    Update DataTable
"""
@app.callback(
    [Output('datatable', 'columns'),
    Output('datatable', 'data')],
    [Input('dataset', 'children'),
    Input('parcoord', 'restyleData'),
    Input('sub_dataset', 'children')],
    [State('scatter', 'figure'),
    State('parcoord', 'figure')]
)
def update_datatable(dataset, parcoord_restyle_data, sub_dataset,
                scatter_figure, parcoord_figure):
    start_time = time.time()
    if dataset is None:
        raise PreventUpdate
    else:
        prop_id = callback_context.triggered[0]['prop_id']
        data = select_dataset(dataset, sub_dataset, parcoord_figure, prop_id)
        rows = data.to_dict('rows')
        columns = [{'name': i, 'id': i, 'selectable': True} for i in data.columns]
        print("datatable: ", time.time() - start_time)
        return columns, rows

"""
    Update the sub_dataset
"""
@app.callback(
    Output('sub_dataset', 'children'),
    [Input('dataset', 'children'),
    Input('scatter', 'selectedData')],
    [State('scatter', 'figure'),
    State('parcoord', 'figure')]
)
def update_sub_dataset(dataset, selected_data, scatter_figure, parcoord_figure):
    if dataset is None:
        raise PreventUpdate
    else:
        prop_id = callback_context.triggered[0]['prop_id']
        if prop_id == 'dataset.children' or selected_data is None:
            return None
        else:
            data = constraint_data(dataset, parcoord_figure)
            sub_dataset = build_sub_dataset(data, selected_data, scatter_figure)
            return sub_dataset

"""
    Update scatter graph

    :input param dataset: a json object of dataset generated by the evaluation
    :input param selected_columns: a list of selected columns in datatable
    :input param restyle_data: data of the last change of constraintrange
    :state param selected_data: a dictionary of selected point in scatter graph
    :state param parcoord_figure: a dictionary of parcoord's figure data
    :state param run_timestamp: an integer represents the time(in ms since 1970) of last click
    :return:
"""
@app.callback(
    [Output('scatter', 'figure'),
    Output('scatter', 'selectedData')],
    [Input('dataset', 'children'),
    Input('datatable', 'selected_columns'),
    Input('parcoord', 'restyleData')],
    [State('scatter', 'selectedData'),
    State('parcoord', 'figure')]
)
def update_scatter(dataset, selected_columns, restyle_data, selected_data, parcoord_figure):
    start_time = time.time()
    if dataset is None or len(selected_columns) == 0:
        raise PreventUpdate
    else:
        prop_id = callback_context.triggered[0]['prop_id']
        if len(selected_columns) < 2 or prop_id == 'dataset.children':
            if selected_data is None:
                return EMPTY_GRAPH, no_update
            else:
                return EMPTY_GRAPH, None
        if restyle_data is not None:
            results = constraint_data(dataset, parcoord_figure)
        else:
            df = json.loads(dataset)
            inputs = pd.read_json(df['inputs'], orient = 'split')
            outputs = pd.read_json(df['outputs'], orient = 'split')
            results = inputs.round(3).join(outputs)
        if selected_columns[1] not in OUTPUT_LIST:
            """To keep the y asix always be an output parameter,
            except both selected columns are input parameters.
            """
            tmp = selected_columns[0]
            selected_columns[0] = selected_columns[1]
            selected_columns[1] = tmp
        fig = {
            'data': [{
                'type': 'scattergl',
                'mode': 'markers',
                'x': results[selected_columns[0]].tolist(),
                'y': results[selected_columns[1]].tolist(),
            }],
            'layout': {
                'xaxis': {'title': selected_columns[0]},
                'yaxis': {'title': selected_columns[1]},
                'clickmode': 'event+select'
            }
        }
        print("scatter: ", time.time() - start_time)
        if selected_data is None:
            """To avoid traggle other callback"""
            return fig, no_update
        else:
            return fig, None

"""Highlight the first two selected columns"""
@app.callback(
    Output('datatable', 'style_data_conditional'),
    [Input('datatable', 'selected_columns')]
)
def highlight_columns(selected_columns):
    return [{
        'if': {'column_id': i},
        'background_color': '#FFFBCC'
    } for i in selected_columns[0:2]]

#helper function
"""A stupid solution for setting fixed input parameter range"""
def change_range(l):
    l[0]['range'] = [0.02, 0.2]
    l[1]['range'] = [0.1, 0.3]
    l[2]['range'] = [0.1, 5]
    l[3]['range'] = [0.01, 0.99]
    l[4]['range'] = [10, 15]
    l[5]['range'] = [10, 15]
    l[6]['range'] = [0.01, 0.99]
    return l

"""Select the dataset that will be used for datatable and return the dataframe"""
def select_dataset(dataset, sub_dataset, parcoord_figure, prop_id):
    if parcoord_figure == EMPTY_GRAPH or prop_id == 'dataset.children':
        df = json.loads(dataset)
        inputs = pd.read_json(df['inputs'], orient = 'split').round(3)
        outputs = pd.read_json(df['outputs'], orient = 'split')
        data = inputs.join(outputs)
    else:
        if sub_dataset is None:
            data = constraint_data(dataset, parcoord_figure)
        else:
            df = json.loads(sub_dataset)
            inputs = pd.read_json(df['inputs'], orient = 'split').round(3)
            outputs = pd.read_json(df['outputs'], orient = 'split')
            data = inputs.join(outputs)
    return data

""" Build a dataset with the selected data from scatter """
def build_sub_dataset(data, selected_data, figure):
    df = pd.DataFrame()
    for i in selected_data['points']:
        df = df.append(data.loc[
            (data[figure['layout']['xaxis']['title']] == i['x']) &
            (data[figure['layout']['yaxis']['title']] == i['y'])])
    dfs = np.split(df, [7], axis=1)
    inputs = dfs[0]
    outputs = dfs[1]
    sub_dataset = {
        'inputs': inputs.to_json(orient = 'split', date_format = 'iso'),
        'outputs': outputs.to_json(orient = 'split', date_format = 'iso')
    }
    return json.dumps(sub_dataset)

"""Return constrainted data from parcoord"""
def constraint_data(dataset, figure):
    df = json.loads(dataset)
    inputs = pd.read_json(df['inputs'], orient = 'split').round(3)
    outputs = pd.read_json(df['outputs'], orient = 'split')
    data = inputs.join(outputs)
    tmp_df = pd.DataFrame()
    for column in figure['data'][0]['dimensions']:
        if 'constraintrange' in column:
            range_list = column['constraintrange']
            #if there is only one constraintrange, range_list is a list of float
            #otherwise, is a list of list
            if isinstance(range_list[0], float):
                range_min = range_list[0]
                range_max = range_list[1]
                tmp_df = tmp_df.append(data.loc[
                    (data[column['label']] >= range_min) &
                    (data[column['label']] <= range_max)])
            else:
                for constraint_range in range_list:
                    range_min = constraint_range[0]
                    range_max = constraint_range[1]
                    tmp_df = tmp_df.append(data.loc[
                        (data[column['label']] >= range_min) &
                        (data[column['label']] <= range_max)])
            data = tmp_df.copy()
            tmp_df = pd.DataFrame()
    return data

if __name__ == '__main__':
    app.run_server(debug=True)

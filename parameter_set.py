from besos.parameters import wwr, RangeParameter, FieldSelector, FilterSelector, GenericSelector, Parameter, expand_plist

def parameter_set(wall_value, attic_value, u_value, solar_value, electric_value, light_value, wwr_value):
    parameters = expand_plist(
        {
            # Wall Materials
            'Mass NonRes Wall Insulation':
                {'Conductivity': (wall_value[0], wall_value[1])},
            'AtticFloor NonRes Insulation': {'Thickness': (attic_value[0], attic_value[1])},
            # Window Materials
            'NonRes Fixed Assembly Window':
                {'U-Factor':(u_value[0], u_value[1]),
                'Solar Heat Gain Coefficient':(solar_value[0], solar_value[1])},
             }
    )
    parameters[0].name = 'Wall conductivity'
    parameters[1].name = 'Attic thickness'
    parameters.append(Parameter(
        selector = FieldSelector(class_name='ElectricEquipment', object_name='*', field_name='Watts per Zone Floor Area'),
        value_descriptor = RangeParameter(min_val= electric_value[0], max_val=electric_value[1])
        ))
    parameters.append(Parameter(
        selector = FieldSelector(class_name='Lights', object_name='*', field_name='Watts per Zone Floor Area'),
        value_descriptor = RangeParameter(min_val= light_value[0], max_val=light_value[1])
        ))
    parameters.append(wwr(RangeParameter(wwr_value[0], wwr_value[1])))
    return parameters
